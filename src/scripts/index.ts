import * as ExTypes from './ex_types/types';

const message = 'TypeScript - ';

const num1 = 101;
const a: boolean = false;
const n1 = 1;
const n2 = 2;
const n3 = {
    apple: n1,
    mango: n2,
};
const n4 = n3.apple * 2;

// document.querySelector('#name').textContent = message + num1;
const element = document.getElementById('name');
if (element != null) {
    element.innerHTML = ExTypes.exercise(1);
    if (a) {
        element.innerHTML = `n1 => ${n1}, n2 => ${n2}
        , n3.apple => ${n3.apple},n3.mango => ${n3.mango}, n4 => ${n4}`;
    }
}
